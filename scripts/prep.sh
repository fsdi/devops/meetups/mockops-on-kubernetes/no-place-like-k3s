#!/bin/bash

CURR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
[ -d "$CURR_DIR" ] || { echo "FATAL: no current dir (maybe running in zsh?)";  exit 1; }

# shellcheck source=./common.sh
source "$CURR_DIR/common.sh"

section "Cleaning up docker environment..."
docker rm -f $(docker ps -qa)
docker network prune -f
docker volume prune -f
docker system prune -a -f

section "Pulling images..."
docker pull rancher/k3s:v1.20.0-k3s2
docker pull rancher/k3d-proxy:v4.1.0
docker pull rancher/k3d-tools:v4.1.0
docker pull python:3.7-slim

section "Preparing Filesystem..."
mkdir /tmp/src

section "Validating installations..."
k3d --version       || echo "please install k3d"
kubectl version     || echo "please install kubectl"
kustomize version   || echo "please install kustomize"
helm version        || echo "please install helm"
tilt version        || echo "please install tilt"
jq --version        || echo "please install jq"
argocd version      || echo "please install argocd-cli"