#!/bin/bash
CURR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )";
[ -d "$CURR_DIR" ] || { echo "FATAL: no current dir (maybe running in zsh?)";  exit 1; }

source "$CURR_DIR/common.sh";

section "$VENDOR Cluster :: Create a Cluster"
# info "Cluster Name: argocd"
# info "--api-port 6550: expose the Kubernetes API on localhost:6550 (via loadbalancer)"
# info "--servers 1: create 1 server node"
# info "--agents 3: create 3 agent nodes"
# info "--port 8080:80@loadbalancer: map localhost:8080 to port 80 on the loadbalancer (used for ingress)"
# info "--wait: wait for all server nodes to be up before returning"
info_pause_exec "Create a cluster" "k3d cluster create argocd --api-port 6550 --servers 1 --agents 2 --port 443:80@loadbalancer --wait"

section "$VENDOR Cluster :: Access the Cluster"

info_pause_exec "List clusters" "k3d cluster list"

info_pause_exec "Update the default kubeconfig with the new cluster details (Optional, included in 'k3d cluster create' by default)" "k3d kubeconfig merge argocd --kubeconfig-merge-default --kubeconfig-switch-context"
# info "Cluster Name: argocd"
# info "--kubeconfig-merge-default true: overwrite existing fields with the same name in kubeconfig (true by default)"
# info "--kubeconfig-switch-context true: set the kubeconfig's current-context to the new cluster context (false by default)"

info_pause_exec "Use kubectl to checkout the nodes" "kubectl get nodes"

section "$VENDOR Cluster :: Install argocd in argocd namespace"
# info It is most common to use argocd as the namspace for argocd

info_pause_exec "Create a new 'argocd' namespace" "kubectl create namespace argocd"
info_pause_exec "Switch to the new 'argocd' namespace" "kubens argocd"
# info make sure kubens is installed 

info_pause_exec "Deploy the argocd via kustomize" "kubectl apply -k assets/kustomize/argocd/"

info_pause_exec "Lets see argo-cd components" "kubectl get po -n argocd"
# info "examine the differnt argocd coponents argocd-server, argocd-repo-server, argocd-application-controller "

info_pause_exec "Lets see argo-cd ingress" "kubectl get ing -n argocd"

section "$VENDOR :: Use ArgoCD"
info_pause_exec "get argocd initial admin password by running" "echo kubectl get secret argocd-initial-admin-secret -n argocd -o json | jq '.data | map_values(@base64d)'"

info_pause_exec "Please note you should use 8080" "open http://argocd.k3d.localhost:8080"

# info "get argocd initial admin password by running"
# info "kubectl get secret argocd-initial-admin-secret -n argocd -o json | jq '.data | map_values(@base64d)'"
