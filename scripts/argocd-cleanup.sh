#!/bin/bash

CURR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
[ -d "$CURR_DIR" ] || { echo "FATAL: no current dir (maybe running in zsh?)";  exit 1; }

# shellcheck source=./common.sh
source "$CURR_DIR/common.sh"

configfile=./assets/k3d-config-registry.yaml

section "Cleanup ArgoCD environment"

info_pause_exec "Deleting the k3d argocd cluster" "k3d cluster delete argocd"