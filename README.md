# Learn K8s via k3d

> TLDR; theres a makefile ;) / README(ies) - make your pick ;)

## :hourglass: Requirements

- [`docker`](https://docs.docker.com/get-docker/)
- [`k3d >= v4.1.0`](https://k3d.io/#installation)
- [`kubectx + kubens`](https://github.com/ahmetb/kubectx) callable via the `kubens` binary
- [`chromium`](https://www.chromium.org/Home) callable via the `chromium` binary
- [`Helm 3`](https://helm.sh/docs/intro/install/)
- [`Kustomize`](https://kubernetes-sigs.github.io/kustomize/installation/)
- [`tilt`](https://docs.tilt.dev/install.html)
- [`argocd`](https://argoproj.github.io/argo-cd/getting_started/#2-download-argo-cd-cli)

## Resources

- <https://k3d.io/>
- <https://github.com/rancher/k3d>
- v4 Release Candidate: <https://github.com/rancher/k3d/releases/tag/v4.1.0>
  - install e.g. via `wget -q -O - https://raw.githubusercontent.com/rancher/k3d/main/install.sh | TAG=v4.1.0 bash`
  
## Run

- Preparation (attention: clears all docker settings!): 

```sh
make prep`
```

### Full Lifecycle with Kustomize

- Demo: Full k3d lifecycle and usage with a Python App using hot-reloading of code: `
 
```sh
make demo-kustomize
```

### Full Lifecycle with Helm

- Demo: Full k3d lifecycle and usage with a Python App using hot-reloading of code: `
 
```sh
make demo-helm
```
### Argocd installation

```sh
make demo-argocd
```

- Cleanup argocd environment with

```sh
make demo-argocd-cleanup
```

### App-Reloading with Tilt and a k3d-managed registry

- Demo: Using k3d with a managed registry and [Tilt](https://tilt.dev) for app-reloading: 

```sh
make demo-tilt
```

- Cleanup tilt environment with

```sh
make demo-tilt-cleanup
```
