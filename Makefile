.PHONY: prep demo-helm demo-kustomize demo-tilt demo-tilt-cleanup demo-argocd demo-argocd-cleanup

export KUBECONFIG=$(PWD)/kubeconfig

demo-helm:
	scripts/lifecycle-helm.sh

demo-kustomize:
	scripts/lifecycle-kustomize.sh

demo-tilt:
	scripts/tilt.sh

demo-tilt-cleanup:
	scripts/tilt-cleanup.sh

demo-argocd:
	scripts/argocd.sh

demo-argocd-cleanup:
	scripts/argocd-cleanup.sh

prep:
	scripts/prep.sh