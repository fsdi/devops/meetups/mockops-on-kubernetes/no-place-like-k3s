# Install ArgoCD

## using kustomize based on git tag:

```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

resources:
  - https://github.com/argoproj/argo-cd/manifests/cluster-install?ref=v2.1.0
```

## Patching argocd-server with --insecure for disabeling TLS

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app.kubernetes.io/component: server
    app.kubernetes.io/name: argocd-server
    app.kubernetes.io/part-of: argocd
  name: argocd-server
spec:
    spec:
      containers:
      - command:
        - argocd-server
        - --redis
        - argocd-redis:6379
        - --insecure
```

## Adding argocd-ingress 

```yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: "argocd-igress"
  namespace: argocd
  labels:
    app.kubernetes.io/component: server
    app.kubernetes.io/name: argocd-server
    app.kubernetes.io/part-of: argocd
spec:
  rules:
    - host: "argocd.k3d.localhost"
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              serviceName: argocd-server
              servicePort: 80
```

### kubectl-convert

```sh
   curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/darwin/amd64/kubectl-convert"
   chmod u+x kubectl-convert
   sudo mv kubectl-convert /usr/local/bin
   kubectl-convert -f ./argocd/ingress-v1beta1.yaml --output-version networking.k8s.io/v1 > ./argocd/ingress.yaml
```