# sample-app - python flask web example

## Build

1. `Dockerfile` is used in the upstream make file to build and import this image as `sample-app:local`, this will help us run the app before pushing to a registry / commiting anyhthing to source control.
1. helm chart `sample/conf/charts/sample-app` is a simplistic example which is desined for k3d + traefik enabled in order to work, nontheless it's a great example of working locally with `helm` and testing the chart before pushing to upstream.
2. kustomize
3. `Makefile` see below

## Make - running locally

This `Makefile` was designed for the developers of the app to run, test, lint and build locally.
- If this is your requirement there is a :hourlass: prequisite of `python3` - the latest minor release e.g `7,8,9` should suffice for this example.

